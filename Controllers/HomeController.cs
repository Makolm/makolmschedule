﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Plan.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Plan.Models;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace Plan.Controllers
{
    public class HomeController : Controller
    {
        public async Task<IActionResult> Main(FireBaseService service)
        {
            ViewData["Title"] = "Главная";

            List<MPlan> plans = await service.GetPlans();
            if (plans == null)
                plans = new List<MPlan>();
            return View(plans);
        }

        [HttpPost]
        public async Task<string> AddPlan(string PlanName, string Description, string[] TasksName, int[] ImportantLevels, bool[] IsDones, FireBaseService service)
        {
            var plan = new MPlan
            {
                PlanName = PlanName,
                Description = Description
            };

            for (int i = 0; i < TasksName.Length; i++)
            {
                plan.Tasks.Add(new MTask { TaskName = TasksName[i], ImportantLevel = ImportantLevels[i], IsDone = Convert.ToBoolean(IsDones[i]) });
            }
        
            service.addTasks(plan);

            return await Task.Run(() => new string($"Good"));
        }

        [HttpPost]
        public async void DeletePlan(string name, FireBaseService service)
        {
            service.DeletePlan(name);
        }

        //[HttpPost]
        //public async Task<string> Registration(string name, string password, FireBaseService service)
        //{
        //    var user = await service.GetUser(name);

        //    if (user != null && user.Name != null)
        //        return await Task.Run(() => new string("User is already exists"));
        //    else
        //    {
        //        // допилить регистрацию
        //        service.RegisterUser(name, new Models.User() { Name = name, Password = password }) ;
        //        var claims = new List<Claim>
        //        {
        //            new Claim(ClaimsIdentity.DefaultNameClaimType, name)
        //        };

        //        ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
        //        await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));

        //        return await Task.Run(() => new string("Good"));
        //    }

        //}

    }
}
