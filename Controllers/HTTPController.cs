﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Plan.Controllers
{
    public class HTTPController : Controller
    {

        public IActionResult Error(int status_code)
        {
            //
            //сделать проверку на существование файла во Views/HTTP/...
            //

            ViewBag.error_code = status_code;
            return View(status_code.ToString());
        }

    }
}
