﻿using FireSharp.Config;
using FireSharp.Interfaces;
using FireSharp.Response;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Plan.Models;
using Newtonsoft.Json.Linq;

namespace Plan.Services
{
    public class FireBaseService
    {

        private IFirebaseConfig config = null;
        private FireSharp.FirebaseClient client = null;
        public readonly bool isConnected = false;

        public FireBaseService()
        {

            var settings = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();

            config = new FirebaseConfig()
            {
                BasePath = settings["ConnectionStrings:DefaultConnection"]
            };

            client = new FireSharp.FirebaseClient(config);

            isConnected = (client != null) ? true : false;

        }

        //public async Task<User> GetUser(string name)
        //{
           
        //    try
        //    {
        //        FirebaseResponse resp;
        //        resp = await client.GetAsync("users/" + name);
             
        //        if((int)resp.StatusCode == 200)
        //            return await Task.Run(() => resp.ResultAs<User>());
        //    }
        //    catch(Exception ex) {
            
        //    }

        //    return await Task.Run(() => (User)null);

        //}

        //public async void RegisterUser(string name, User user)
        //{
        //    try
        //    {
        //        SetResponse resp;
        //        resp = await client.SetAsync("users/" + name, user);
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}

        public async void addTasks(MPlan plan)
        {

            try
            {
                SetResponse resp;
                resp = await client.SetAsync("plans/" + plan.PlanName + "/", plan);
            }
            catch (Exception)
            {

            }

        }

        public async Task<List<MPlan>> GetPlans()
        {
            try
            {
                FirebaseResponse resp;
                resp = await client.GetAsync("plans/");
                var json_plans = JObject.Parse(resp.Body);
                List<MPlan> plans = new List<MPlan>();

                foreach (var plan in json_plans)
                {
                    plans.Add(plan.Value.ToObject<MPlan>());
                    
                }
               
                if ((int)resp.StatusCode == 200)
                    return await Task.Run(() => plans);
            }
            catch (Exception ex)
            {

            }

            return await Task.Run(() => (List<MPlan>)null);
        }

        public async void DeletePlan(string planName)
        {
            FirebaseResponse resp;
            resp = await client.DeleteAsync("plans/" + $"{planName}/");
        }

        public async void AddData(string a, string b)
        {
            FirebaseResponse resp;
            resp = await client.SetAsync(a, b);
        }

    }
}
