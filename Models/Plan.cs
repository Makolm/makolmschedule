﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Plan.Models
{
    public class MTask
    {
        public string TaskName;
        public int ImportantLevel;
        public bool IsDone;
    }

    public class MPlan
    {
        public string PlanName;
        public string Description;
        public List<MTask> Tasks = new List<MTask>();
    }
}
