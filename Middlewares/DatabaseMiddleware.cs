﻿using Microsoft.AspNetCore.Http;
using Plan.Services;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Plan.Middlewares
{
    public class DatabaseMiddleware
    {

        protected readonly RequestDelegate _next;

        public DatabaseMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context, FireBaseService firebase)
        {
            if (!firebase.isConnected)
                context.Response.StatusCode = 404;
            else
                await _next.Invoke(context);
        }

    }
}
