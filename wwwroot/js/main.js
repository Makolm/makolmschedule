﻿document.addEventListener("DOMContentLoaded", function () {

    //const enter = document.querySelector('.right_header_section');
    //const cross = document.querySelector('#cross');

    //const user_section = document.querySelector('#user_control_section');
    //const left_section = document.querySelector('.authorization_section');
    //const right_section = document.querySelector('.registration_section');
    //const MS_logo = document.querySelector('#MS_logo');

    //const before_left = 'before_door_animation_left';
    //const before_right = 'before_door_animation_right';
    //const after_left = 'after_door_animation_left';
    //const after_right = 'after_door_animation_right';

    //const show_logo_anim = 'to_show_animation';
    //const hide_logo_anim = 'to_hide_animation';
    //const logo_anim = 'MS_logo_animation';

    //const limit_ms = 500;
    //var time_defend = Date.now();

    //enter.addEventListener('click', () => {

    //    if ((time_defend + limit_ms * 2) > Date.now())
    //        return;
    //    else
    //        time_defend = Date.now();

    //    user_section.style.display = 'block';
    //    left_section.classList.add(before_left);
    //    right_section.classList.add(before_right);

    //    setTimeout(() => {
    //        MS_logo.style.display = 'flex';
    //        MS_logo.classList.add(show_logo_anim);

    //        setTimeout(() => {
    //            MS_logo.classList.add(logo_anim);
    //        }, limit_ms);

    //    }, limit_ms);
        
    //});

    //cross.addEventListener('click', () => {

    //    if ((time_defend + limit_ms * 2) > Date.now())
    //        return;
    //    else
    //        time_defend = Date.now();

    //    MS_logo.classList.remove(logo_anim);
    //    MS_logo.classList.add(hide_logo_anim);

    //    setTimeout(() => {
    //        MS_logo.classList.remove(show_logo_anim);
    //        MS_logo.classList.remove(hide_logo_anim);

    //        left_section.classList.add(after_left);
    //        right_section.classList.add(after_right);
    //        MS_logo.style.display = 'none';

    //        setTimeout(() => {
    //            left_section.classList.remove(before_left);
    //            right_section.classList.remove(before_right);
    //            left_section.classList.remove(after_left);
    //            right_section.classList.remove(after_right);

    //            MS_logo.classList.remove(logo_anim);
    //            MS_logo.classList.remove(show_logo_anim);
    //            MS_logo.classList.remove(hide_logo_anim);

    //            user_section.style.display = 'none';
    //        }, limit_ms);

    //    }, limit_ms);

    //});

    ////разбить по файлам анимацию и отклики
    ////вместо констов, сделать 1 объект

    //const register = document.querySelector('#register_submit');
    //const register_form = document.querySelector('.registration_section > form');
    //const register_way = register_form.getAttribute('action');

    //register.addEventListener('click', async (evt) => {
    //    evt.preventDefault();

    //    let result = await (await fetch(register_way, {
    //        method: 'POST',
    //        body: new FormData(register_form),
    //    })).text();

    //    if (result == "Good") {
    //        window.location.reload();
    //    }

    //    console.log(result);
    //});

    const add_plan = document.querySelector('#create_plan_button');
    const add_task = document.querySelector('#add_task');
    const form = document.querySelector('#add_plan_form');
    const button_add = document.querySelector('#add_task');
    const eclipse = document.querySelector('#eclipse');
    const add_plan_element = document.querySelector('.add_plan');
    const cross_eclipse = document.querySelector('.cross_eclipse');

    let control_delete = {
        old_name: null,
        name: null
    }

    add_task.addEventListener('click', () => {
        AddTaskField();
    });

    add_plan.addEventListener('click', async (evt) => {

        evt.preventDefault();

        let result = await (await fetch('/Home/AddPlan', {
            method: "POST",
            body: new FormData(form)
        })).text();

        if (result == "Good") {
            control_delete.name = form.querySelector('input[name="PlanName"]').value;
            if (control_delete.old_name != null && control_delete.old_name != control_delete.name) {
                let div = document.createElement('div');
                div.innerHTML = '<form><input name="name" value="' + control_delete.old_name + '"/><button></button></form>';
                console.log(div);
                await fetch('/Home/DeletePlan', {
                    method: "POST",
                    body: new FormData(div.querySelector('form'))
                });
            }

            window.location.reload();
        }

    });

    add_plan_element.addEventListener('click', () => {

        eclipse.style.display = 'flex';
        eclipse.classList.add('to_show_animation');
    });

    cross_eclipse.addEventListener('click', () => {
        eclipse.classList.add('to_hide_animation');

        setTimeout(() => {
            eclipse.classList.remove('to_hide_animation');
            eclipse.classList.remove('to_show_animation');
            eclipse.style.display = 'none';

            control_delete.old_name = null;
            let elements = form.querySelectorAll('.task');
            for (let i = 0; i < elements.length; i++) {
                elements[i].remove();
            }
        }, 500);
    });

    document.addEventListener('click', (evt) => {
        var element = document.elementFromPoint(evt.clientX, evt.clientY);
        var result = element.closest('.plan');

        if (result) {
            var mas = result.querySelectorAll('.task_div');
            let name = result.querySelector('.add_plan_header').textContent;
            let description = result.querySelector('.add_plan_description span').textContent;

            var last_task;
            var task_name, task_name, task_level, task_done;

            control_delete.old_name = name;
            form.querySelector('input[name="PlanName"]').setAttribute('value', name);
            form.querySelector('input[name="Description"]').setAttribute('value', description);
            for (let i = 0; i < mas.length; i++) {
                task_name = mas[i].querySelector('.task_name').textContent;
                task_level = mas[i].querySelector('.task_level').textContent;
                task_done = mas[i].querySelector('.task_done').textContent;

                AddTaskField();
                last_task = form.querySelector('.task:last-of-type');

                last_task.querySelector('input[name="TasksName"]').setAttribute('value', task_name);
                last_task.querySelector('select[name="ImportantLevels"]').setAttribute('value', task_level);
                last_task.querySelector('input[name="isDones"]').setAttribute('value', task_done);
            }

            add_plan_element.click();
        }

        result = element.closest('.delete_task');

        if (result) {
            element.closest('.task').remove();
        }
    });

    function AddTaskField() {
        let
            element =
                '<div class="task just_around">'
                + '<input placeholder="Имя задачи" type="text" class="makolm_input makolm_transition" name="TasksName" value=""/>'
                + '<select name="ImportantLevels">'
                + '<option value="1">Можно и не делать, но можно и сделать</option>'
                + '<option value="2">Желательно сделать</option>'
                + '<option value="3">Нужно сделать</option>'
                + '<option value="4">Обязательно нужно сделать</option>'
                + '<option value="5">Приступить к выполнению задания немедленно</option>'
                + '</select>'
                + '<input placeholder="Имя задачи" type="text" class="makolm_input makolm_transition" name="isDones" value="false"/>'
                + '<div class="makolm_transition delete_task"></div>'
                + '</div>';

        button_add.insertAdjacentHTML('beforebegin', element);
    }

})