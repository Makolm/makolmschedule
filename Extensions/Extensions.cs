﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Plan.Middlewares;
using Plan.Services;
using System.Threading.Tasks;

namespace Plan.Extensions
{
    public static class Extensions
    {
        public static IApplicationBuilder UseMSFirebase(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<DatabaseMiddleware>();
        }

        public static IApplicationBuilder UseMSAuntefication(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<AunteficationMiddleware>();
        }

        public static IServiceCollection AddFirebase(this IServiceCollection builder)
        {
            return builder.AddSingleton<FireBaseService>();
        }

    }
}
